import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Rutas
import { APP_ROUTING } from './app.routes';

// Servicios
// import { HeroesService } from './servicios/heroes.service';


//primefaces
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; // Animaciones
import {TabViewModule} from 'primeng/tabview'; // Vista de tablas
import {FieldsetModule} from 'primeng/fieldset'; // 
import {TooltipModule} from 'primeng/tooltip';
import {CardModule} from 'primeng/card';



// Componentes
import { AppComponent } from './app.component';
// import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';


//shared components
import { SlideComponent } from './components/slide/slide.component';
import { AboutComponent } from './components/about/about.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { ServicesComponent } from './components/services/services.component';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    SlideComponent,
    AboutComponent,
    FooterComponent,
    ServicesComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    APP_ROUTING,
    AccordionModule,
    BrowserAnimationsModule,
    TabViewModule,
    FieldsetModule,
    TooltipModule,
    CardModule
    ],
  providers: [
    // HeroesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
